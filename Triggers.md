# Trigger

- In the context to databases, a trigger is a database object that is automatically executed in response to a specific event or action. 
- Triggers are used to enforce data integrity, implement complex business logic, and automate actions within a database. 
- They are typically defined to respond to events like data modifications (inserts, updates, deletes) or certain database operations.

Key characteristics of database triggers include:

### Event-Based Execution: 
- Triggers are executed automatically when specific events occur. 
- The events can be data-related, like an insert, update, or delete operation on a table, or they can be system-related events, such as database startup or shutdown.

### Timing: 
- Triggers can be defined to execute either before the event (BEFORE trigger) or after the event (AFTER trigger). 
- BEFORE triggers are often used for data validation or modification before the event occurs, while AFTER triggers are typically used for post-event actions or auditing.

Row-Level or Statement-Level: Triggers can be defined to operate at either the row level or the statement level. Row-level triggers execute once for each affected row, while statement-level triggers execute once for each SQL statement that triggers the event.

Event and Trigger Activation: Triggers can be associated with tables, views, or databases. They are activated automatically when the defined event occurs.

