# Database Isolation

- It defines the degree to which a transaction must be isolated from the data modifications made by any other transaction(even though in reality there can be a large number of concurrently running transactions). 
- The overarching goal is to prevent reads and writes of temporary, aborted, or otherwise incorrect data written by concurrent transactions. 

The standard SQL isolation levels, as defined by the SQL standard, include:

## Read Uncommitted:

This is the lowest isolation level.
Transactions can read uncommitted changes made by other transactions.
There is a high risk of dirty reads, non-repeatable reads, and phantom reads.
It offers the highest level of concurrency but the lowest data integrity.

## Read Committed:

This level allows transactions to read only committed data.
It prevents dirty reads but still allows non-repeatable reads and phantom reads.
It is suitable for scenarios where data consistency is essential, but some level of concurrency is still required.

## Repeatable Read:

In this isolation level, a transaction sees a consistent snapshot of the data throughout its lifetime.
It prevents dirty reads and non-repeatable reads but still allows phantom reads.
It is suitable for scenarios where data consistency is critical, and non-repeatable reads need to be avoided.

## Serializable:

This is the highest isolation level.
It provides the strongest data consistency by ensuring that transactions are executed serially, even if it affects performance.
It prevents dirty reads, non-repeatable reads, and phantom reads, making it suitable for applications with strict data consistency requirements