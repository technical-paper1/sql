# Joins

In context to SQL, `'Join'` is an operation which is used when we need to combine two or more columns of tables based on the related column between them.

There are several types of joins in SQL:

## Inner Join : 

- Returns only the rows that have matching values in both tables.
- Non-matching rows from both tables are excluded from the result.
- Syntax: 
  ```sql
  SELECT ... FROM table1 INNER JOIN table2 ON table1.column = table2.column
  ```

<p align="center">
  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToQ6N8_DsVr6f45ilj4HCHw-SQHZXMfVVbZw&usqp=CAU" />
</p>

## LEFT JOIN (or LEFT OUTER JOIN):

- Returns all rows from the left table and matching rows from the right table.
- If there is no match, NULL values are used for the right table's columns.
- Syntax: 
  
  ```sql
  SELECT ... FROM table1 LEFT JOIN table2
   ON table1.column = table2.column
  ```
<p align="center">
  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqK2frrL42_ivXwmxTwBAqSCebSc3rr9F9fA&usqp=CAU" />
</p>

## RIGHT JOIN (or RIGHT OUTER JOIN):

- The reverse of the LEFT JOIN.
- Returns all rows from the right table and matching rows from the left table.
- Non-matching rows from the left table contain NULL values.
- Syntax: 
  ```sql
  SELECT ... FROM table1 RIGHT JOIN table2
   ON table1.column = table2.column
  ```

## FULL JOIN (or FULL OUTER JOIN):
- Returns all rows when there is a match in either the left or right table.
- If there is no match, NULL values are used.
- Syntax: 
  ```sql
  SELECT ... FROM table1 FULL JOIN table2
   ON table1.column = table2.column
  ```

## SELF JOIN:

- A self join is used when you want to combine rows from a single table.
- It can be used to compare rows within the same table by creating aliases for the table.
- Syntax: 
  ```sql
  SELECT ... FROM table AS t1 INNER JOIN table AS t2
   ON t1.column = t2.column
  ```