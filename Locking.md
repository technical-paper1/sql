# Locking 

- Locking protocols are used in database management systems as a means of concurrency control. 
- Multiple transactions may request a lock on a data item simultaneously. 
- Hence, we require a mechanism to manage the locking requests made by transactions. Such a mechanism is called a Lock Manager. 
- It relies on the process of message passing where transactions and lock manager exchange messages to handle the locking and unlocking of data items. 

## Advantages of Locking

### Data Consistency: 
  - Locking can help ensure data consistency by preventing multiple users from modifying the same data simultaneously. 
  - By controlling access to shared resources, locking can help prevent data conflicts and ensure that the database remains in a consistent state.
### Isolation: 
- Locking can ensure that transactions are executed in isolation from other transactions, preventing interference between transactions and reducing the risk of data inconsistencies.
### Granularity: 
- Locking can be implemented at different levels of granularity, allowing for more precise control over shared resources. 
- For example, row-level locking can be used to lock individual rows in a table, while table-level locking can be used to lock entire tables.
### Availability: 
- Locking can help ensure the availability of shared resources by preventing users from monopolizing resources or causing resource starvation.


## Disadvantages of Locking

### Overhead: 
- Locking requires additional overhead, such as acquiring and releasing locks on shared resources. 
- This overhead can lead to slower performance and increased resource consumption, particularly in systems with high levels of concurrency.
### Deadlocks: 
- Deadlocks can occur when two or more transactions are waiting for each other to release resources, causing a circular dependency that can prevent any of the transactions from completing. 
- Deadlocks can be difficult to detect and resolve and can result in reduced throughput and increased latency.
  
### Reduced Concurrency: 
- Locking can limit the number of users or applications accessing the database simultaneously. 
- This can lead to reduced concurrency and slower performance in systems with high levels of concurrency.
### Complexity: 
- Implementing locking can be complex, particularly in distributed systems or in systems with complex transactional logic. 
- This complexity can lead to increased development and maintenance costs.