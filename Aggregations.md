# Aggregations in SQL

An Aggregate function is a function where the values of multiple rows are grouped together as input on certain criteria to form a single value of more significant meaning.

There are various types of Aggregate functions.

Some of them are as follows,
- sum()
- count()
- avg()
- min()
- max()

## Count():

- Count(*): Returns total number of records
- Count(column): Return number of Non Null values over the column 
- Count(Distinct column):Return number of distinct Non Null values over the column 

## Sum():

- sum(column):  Sum all Non Null values of Column
- sum(Distinct column): Sum of all distinct Non-Null values

## Avg():

- Avg(column) = Sum(column) / count(column)
- Avg(Distinct column) = sum(Distinct column) / Count(Distinct column)

## Min():

- Min(column): Minimum value in the column column except NULL
- Max(column): Maximum value in the column

# Filtering in SQL

Filtering in SQl allows you to retrieve specific rows from a database table based on certain conditions.

## WHERE Clause:
- This is used to specify conditions that rows must meet to be included in the result set. 
- You place the WHERE clause after the SELECT statement and before other clauses like FROM or JOIN.
    ```sql
    SELECT column1, column2
    FROM table_name
    WHERE condition;
    ```

## Comparisons (=, <, >, <=, >=, <>)
- Logical operators (AND, OR, NOT)
- NULL comparisons (IS NULL, IS NOT NULL)
- Pattern matching (LIKE, IN, BETWEEN, etc.)


    ```sql
    SELECT product_name, price
    FROM products
    WHERE category = 'Electronics' AND price < 500;
    ```

You can use logical operators (AND, OR, NOT) to combine multiple conditions in a WHERE clause. This allows you to create complex filtering rules.
```sql
SELECT employee_name, salary
FROM employees
WHERE department = 'Sales' OR (department = 'Marketing' AND salary > 60000);
```
