# SQL Indexes

- It is a schema object, which is used by the server to speed up the retrieval of rows by using a pointer.
- It helps us to speed up 'select' queiries and 'where' clauses, but at the same time slows down data input.

Syntax to create an index,

```sql
CREATE INDEX some_index
ON TABLE column;
```

- If you need to create multiple indexes for multiple columns, we can use the following syntax for that,
```sql
CREATE INDEX index
on TABLE (c1,c2,c3,....)
```

To remove an Index,
```sql
DROP INDEX index_name;
```

- SQL indexes are used when the table is large and data retreivals are too large.
- When the table needs to be updated frequently.
  