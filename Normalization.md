# Normalization

- Normalization is the process of minimizing redundancy from a relation or set of relations. Redundancy in relation may cause insertion, deletion, and update anomalies.
- It helps to minimize the redundancy in relations. 
- Normal forms are used to eliminate or reduce redundancy in database tables.

There are several types of normalization with their own set of rules.

Some of them are, 

### First Normal Form (1NF): 
  - This is the most basic level of normalization. In 1NF, each table cell should contain only a single value, and each column should have a unique name. 
  - The first normal form helps to eliminate duplicate data and simplify queries.
### Second Normal Form (2NF): 
  - 2NF eliminates redundant data by requiring that each non-key attribute be dependent on the primary key. 
  - This means that each column should be directly related to the primary key, and not to other columns.


### Third Normal Form (3NF): 
  - 3NF builds on 2NF by requiring that all non-key attributes are independent of each other. 
  - This means that each column should be directly related to the primary key, and not to any other columns in the same table.


### Boyce-Codd Normal Form (BCNF): 

  - BCNF is a stricter form of 3NF that ensures that each determinant in a table is a candidate key. 
  - In other words, BCNF ensures that each non-key attribute is dependent only on the candidate key.


### Fourth Normal Form (4NF):
- 4NF is a further refinement of BCNF that ensures that a table does not contain any multi-valued dependencies.
### Fifth Normal Form (5NF):
- 5NF is the highest level of normalization and involves decomposing a table into smaller tables to remove data redundancy and improve data integrity.


Normal forms help to reduce data redundancy, increase data consistency, and improve database performance. However, higher levels of normalization can lead to more complex database designs and queries. It is important to strike a balance between normalization and practicality when designing a database.