# ACID Properties of Databases

- To start with ACID properties of databases we first need to know about what a Transaction is.

## Transaction

- When a set of logical statements are done against a unit of time, it combinedly is called as a Transaction.
- For example,
  -  lets say we need to send some money from our bank account to out friend's bank account.
  -  The task from checking if the required amount is available in out account, to debeting money from our account, then crediting it in to our friend's account, it whole combinedly is called as a `Transaction`.

## ACID

- A - Atomicity
- C - Consistency
- I - Isolation
- D - Durability

## Atomicity :
- We consider the whole set of logical statements as a single task.
- So, we ensure that either the whole task is done, or none is done.
- We dont make it to be in the middle of the task, even if it is we just revert back to the previous task.
- In context to our bank transaction example,
  - Lets say, money has been deducted from our account, but not credited into our friend's account, then we cancel the whole transaction.

## Consistency :
- We ensure the whole integrity of the whole system is the same before the transaction and after the transaction.
- Let's say that we have $200 in out account and some account has $50, now $100 is deducted from our account and credited to that some other account then the whole money before the transaction is the same as the whole money after the transaction i.e. $250.

## Isolation:
- This property ensures that multiple transactions can occur concurrently without leading to the inconsistency of the database state. 
- Transactions occur independently without interference. 
- Changes occurring in a particular transaction will not be visible to any other transaction until that particular change in that transaction is written to memory or has been committed. 
- This property ensures that the execution of transactions concurrently will result in a state that is equivalent to a state achieved these were executed serially in some order. 

## Durability: 
- This property ensures that once the transaction has completed execution, the updates and modifications to the database are stored in and written to disk and they persist even if a system failure occurs.
-  These updates now become permanent and are stored in non-volatile memory. The effects of the transaction, thus, are never lost. 
