# Transactions

- In simple terms, when we treat a whole set of statements like they can be sql statements or logical statements as a single unit, they are called as Transactions.

- For example,
  - Let's say we need to send some money to our friend.
  - Then, we need to perform a set of tasks to complete the whole thing.
  - Like, the system should check if the required amount is present in out account or not.
  - Then, send the money out to the other account.
  - After all this is done, the whole system should maintain integrity and consistency.
  - Which would mean the whole money before and after the transaction is done should be the same.
  
  The following commands are used to maintain Transactions in SQL

### BEGIN TRANSACTION Command
- It indicates the start point of an explicit or local transaction. 

Syntax:
```sql
BEGIN TRANSACTION transaction_name ;
```
### SET TRANSACTION Command
- The values for the properties of the current transaction, such as the transaction isolation level and access mode, are set using the SET TRANSACTION Statement in MySQL.

Syntax: 
```sql
SET TRANSACTION [ READ WRITE | READ ONLY ];
```
### COMMIT Command
- If everything is in order with all statements within a single transaction, all changes are recorded together in the database is called committed. 
- The COMMIT command saves all the transactions to the database since the last COMMIT or ROLLBACK command. 

Syntax:
```sql
COMMIT;
```